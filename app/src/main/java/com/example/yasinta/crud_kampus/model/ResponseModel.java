package com.example.yasinta.crud_kampus.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;
@SuppressWarnings("unused")

public class ResponseModel {
    List<DataModel> result;
    @SerializedName("id")
    private String mId;
    @SerializedName("pesan")
    private String mPesan;
    public List<DataModel> getResult() {
        return result;
    }
    public void setResult(List<DataModel> result) {
        this.result = result;
    }

    public String getId() {
        return mId;
    }
    public void setId(String id) {
        mId = id;
    }
    public String getPesan() {
        return mPesan;
    }
    public void setPesan(String pesan) {
        mPesan = pesan;
    }
}
