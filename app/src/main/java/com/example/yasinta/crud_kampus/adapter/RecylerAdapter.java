package com.example.yasinta.crud_kampus.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.example.yasinta.crud_kampus.MainActivity;
import com.example.yasinta.crud_kampus.R;
import com.example.yasinta.crud_kampus.model.DataModel;
import java.util.List;

public class RecylerAdapter extends
    RecyclerView.Adapter<RecylerAdapter.MyHolder> {List<DataModel> mList ;
        Context ctx;
    public RecylerAdapter(Context ctx, List<DataModel> mList) {
            this.mList = mList;
            this.ctx = ctx;
        }
        @Override
        public RecylerAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType){
            View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist, parent, false);
            MyHolder holder = new MyHolder(layout);
            return holder;
        }
        @Override
        public void onBindViewHolder(RecylerAdapter.MyHolder holder,
        final int position) {
        holder.nama_kampus.setText(mList.get(position).getnama_kampus());
        holder.alamat.setText(mList.get(position).getalamat());
        holder.nhp_kampus.setText(mList.get(position).getnhp_kampus());
        holder.jml_mhs.setText(mList.get(position).getjml_mhs());
        holder.biaya.setText(mList.get(position).getbiaya());
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent goInput = new Intent(ctx,MainActivity.class);
                try {
                    goInput.putExtra("id",
                            mList.get(position).getId());
                    goInput.putExtra("nama_kampus",
                            mList.get(position).getnama_kampus());
                    goInput.putExtra("alamat",
                            mList.get(position).getalamat());
                    goInput.putExtra("nhp_kampus",
                            mList.get(position).getnhp_kampus());
                    goInput.putExtra("jml_mhs",
                            mList.get(position).getjml_mhs());
                    goInput.putExtra("biaya",
                            mList.get(position).getbiaya());
                    ctx.startActivity(goInput);
                }catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(ctx, "Error data " + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
        @Override
        public int getItemCount() { return mList.size(); }
        public class MyHolder extends RecyclerView.ViewHolder {
            TextView nama_kampus, alamat, nhp_kampus, jml_mhs, biaya;
            DataModel dataModel;
            public MyHolder(View v)
            {
                super(v);
                nama_kampus = (TextView) v.findViewById(R.id.tvnama_kampus);
                alamat = (TextView) v.findViewById(R.id.tvalamat);
                nhp_kampus = (TextView) v.findViewById(R.id.tvnhp_kampus);
                jml_mhs = (TextView) v.findViewById(R.id.tvjml_mhs);
                biaya = (TextView) v.findViewById(R.id.tvbiaya);
            }
        }

    }





