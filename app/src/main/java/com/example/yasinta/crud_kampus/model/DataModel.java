package com.example.yasinta.crud_kampus.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DataModel {

    @SerializedName("id")
    private String mId;
    @SerializedName("nama_kampus")
    private String mnama_kampus;
    @SerializedName("alamat")
    private String malamat;
    @SerializedName("nhp_kampus")
    private String mnhp_kampus;
    @SerializedName("jml_mhs")
    private String mjml_mhs;
    @SerializedName("biaya")
    private String mbiaya;


    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getnama_kampus() {
        return mnama_kampus;
    }

    public void setnama_kampus(String nama_kampus) {
        mnama_kampus = nama_kampus;
    }

    public String getalamat() {
        return malamat;
    }

    public void setalamat(String alamat) {
        malamat = alamat;
    }

    public String getnhp_kampus() {
        return mnhp_kampus;
    }

    public void setnhp_kampus(String nhp_kampus) {
        mnhp_kampus = nhp_kampus;
    }

    public String getjml_mhs() {
        return mjml_mhs;
    }

    public void setjml_mhs(String jml_mhs) {
        mjml_mhs = jml_mhs;
    }

    public String getbiaya() {
        return mbiaya;
    }

    public void setbiaya(String biaya) {
        mbiaya = biaya;
    }
}
