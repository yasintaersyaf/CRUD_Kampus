package com.example.yasinta.crud_kampus.api;

import com.example.yasinta.crud_kampus.model.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface RestApi {
    //insert
    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponseModel> sendKelas(@Field("nama_kampus") String nama_kampus,
                                  @Field("alamat") String alamat,
                                  @Field("nhp_kampus") String nhp_kampus,
                                  @Field("jml_mhs") String jml_mhs,
                                  @Field("biaya") String biaya);
    //read
    @GET("read.php")
    Call<ResponseModel> getBiodata();
    //update menggunakan 3 parameter
    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> updateData( @Field("id") String id,
                                    @Field("nama_kampus") String nama_kampus,
                                    @Field("alamat") String alamat,
                                    @Field("nhp_kampus") String nhp_kampus,
                                    @Field("jml_mhs") String jml_mhs,
                                    @Field("biaya") String biaya);
    //delete menggunakan parameter id
    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponseModel> deleteData(@Field("id") String id);
}
